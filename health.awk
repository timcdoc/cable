BEGIN {
    sstr = "date +\"%Y%m%d%H%M%S\""
    sstr | getline foo
    sstr = "date +\"%Y%m%d\""
    sstr | getline fnymd
    fn = "/var/log/cable/" fnymd ".log"
} {
    if ( $0 ~ /dBmV/ ) {
        gsub( />-/, "> -", $0 )
        ch = $1
        sub( /^.*<tr><td>/, "", ch )
        sub( /<\/td>\.*$/, "", ch )
        ch = 0+ch
        adBmV[ch] = 0.0+$3
    }
} END {
    for ( ch = 1; ch <= 32; ch++ ) {
        printf( "%s\t%d\t%s\n", foo, ch, adBmV[ch] ) >>fn
    }
}

