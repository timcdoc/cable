#!
if [ $# -eq 0 ]; then
    echo usage:$0 http_weather_path/weather/daily.htm
    exit 1
fi

for ((;;))
do
    for i in /var/log/cable/*.log
    do
        thisdate=`date +"%Y%m%d"`
        if [ "/var/log/cable/$thisdate.log" == "$i" ]; then
            echo $i is today, partial results only.
            awk -f merge.awk "$i" >"${i/.log/.hourly.part}"
        else
            if [ -f "${i/.log/.hourly.part}" ]; then
                rm "${i/.log/.hourly.part}"
            fi
            if [ -f "${i/.log/.hourly.txt}" ]; then 
                echo skipping $i, already done
            else
                awk -f merge.awk "$i" >"${i/.log/.hourly.txt}"
            fi
        fi
    done
    awk -f graph.awk /var/log/cable/2*.txt /var/log/cable/2*.part
    for i in /var/log/cable/event.*.txt
    do
        if [ -f "$i" ]; then
            awk -f graphevent.awk $i
        fi
    done
    thisdate=`date +"%Y%m%d"`
    if [ -f "/var/log/weather/$thisdate.part" ]; then
        echo skipping /var/log/weather/$thisdate.part already done for the day
        sleep 1000
    else
        echo doing /var/log/weather/$thisdate.part 
        echo curl $1 
        curl $1 2>junk2.tmp >weather.log
        awk -f weather.awk weather.log
        for i in /var/log/weather/*.part
        do
            if [ "/var/log/weather/$thisdate.part" != "$i" ]; then
                if [ -f "${i/.log/.part}" ]; then
                    rm "${i/.log/.part}"
                fi
            fi
        done
        sleep 1000
    fi
    ls /var/www/html | grep "event.*.svg" | sort -n -r >events.sorted.txt
    awk -f events.awk events.sorted.txt >/var/www/html/events.html
done
