BEGIN {
   datetime=ENVIRON["DATETIME"]
   FS="\t"
} {
    printf( "%s\t%s\t%s\n", datetime, "" ( 8*$6), "" ( 8*$7) )
}
