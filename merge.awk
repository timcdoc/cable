{
    namehr = $1
    ch = 0+$2
    db = 0.0+$3
    sub( /[0-9][0-9][0-9][0-9]$/, "", namehr )
    hr = namehr
    nameday = namehr
    sub( /^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/, "", hr )
    sub( /[0-9][0-9]$/, "", nameday )
    hr = 0+hr
    asum[hr,ch] += db
    if ( (hr,ch) in amax ) {
        if ( db > amax[hr,ch] ) {
            amax[hr,ch] = db
        }
    } else {
        amax[hr,ch] = -1000.0
    }
    if ( (hr,ch) in amin ) {
        if ( db < amin[hr,ch] ) {
            amin[hr,ch] = db
        }
    } else {
        amin[hr,ch] = 1000.0
    }
    ac[hr,ch]++
} END {
    for ( hr = 0; hr < 24; hr++ ) {
        for ( ch = 1; ch <= 32; ch++ ) {
            if ( (hr,ch) in ac ) {
                printf( "%s %2.2d %d %3.2f %3.2f %3.2f\n", nameday, hr, ch, amin[hr,ch], asum[hr,ch]/ac[hr,ch], amax[hr,ch] )
            }
        }
    }
}

