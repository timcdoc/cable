BEGIN {
    state = 0
} {
    if ( ( $0 ~ /Today's/ ) && ( $0 ~ / Data File/ ) ) {
        state++
        fnext = "part"
    } else if ( ( $0 ~ /Yesterday's/ ) && ( $0 ~ / Data File/ ) ) {
        state++
        fnext = "log"
    } else if ( state == 1 ) {
        state++
    } else if ( ( state == 2 ) && ( $0 ~ /<pre>/) ) {
        state++
        mo=$0
        sub( /.*>/, "", mo )
        dy=mo
        sub( /\/.*$/, "", mo )
        sub( /[0-9][0-9]*\//, "", dy )
        yr=dy
        sub( /\/.*$/, "", dy )
        sub( /[0-9][0-9]*\//, "", yr )
        yr=0+yr
        dy=0+dy
        mo=0+mo
        if ( yr > 18 ) {
            yr += 2000
        } else {
            yr += 2100
        }
        fn = sprintf("/var/log/weather/%4.4d%2.2d%2.2d.%s", yr, mo, dy, fnext )
    } else if ( state == 3 ) {
        state++
    } else if ( state == 4 ) {
        state++
    } else if ( state == 5 ) {
        if ( $1 ~ /<\/pre>/ ) {
            state = 0
        } else {
            hr = $1
            min = hr
            sub( /:.*$/, "", hr )
            sub( /^.*:/, "", min )
            if ( $1 == "0:00" ) {
                printf( "%4.4d%2.2d%2.2d%2.2d%2.2d %s %s %s %s %s %s\n", yr, mo, dy, hr, min, $6, $8, $10, $13, $14, $15 ) >fn
                close( fn )
            } else {
                printf( "%4.4d%2.2d%2.2d%2.2d%2.2d %s %s %s %s %s %s\n", yr, mo, dy, hr, min, $6, $8, $10, $13, $14, $15 ) >>fn
                close( fn )
            }
        }
    }
}
