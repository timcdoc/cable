for ((;;))
  do
  thisdate=`date +"%Y%m%d"`
  export DATETIME=`date +"%Y%m%d %H%M%S"`
  speedtest -f tsv >speedtest.tmp || echo 0 0 0 0 0 0 0 0 0 >speedtest.tmp
  awk -f speedtest.awk speedtest.tmp >>/var/log/speedtest/$thisdate.hourly.log
  sleep 600
done
