BEGIN {
    dpm[1]=30
    dpm[2]=27
    dpm[3]=30
    dpm[4]=29
    dpm[5]=30
    dpm[6]=29
    dpm[7]=30
    dpm[8]=30
    dpm[9]=29
    dpm[10]=30
    dpm[11]=29
    dpm[12]=30
    mo["Jan"]=1
    mo["Feb"]=2
    mo["Mar"]=3
    mo["Apr"]=4
    mo["May"]=5
    mo["Jun"]=6
    mo["Jul"]=7
    mo["Aug"]=8
    mo["Sep"]=9
    mo["Oct"]=10
    mo["Nov"]=11
    mo["Dec"]=12
    startdate = "" ARGV[ARGC-4]
    starthr = sprintf( "%3.3d", (0+ARGV[ARGC-3])*6 )
    findate = "" ARGV[ARGC-2]
    finhr = sprintf( "%3.3d", (0+ARGV[ARGC-1])*6 )
    delete ARGV[ARGC-4]
    delete ARGV[ARGC-3]
    delete ARGV[ARGC-2]
    delete ARGV[ARGC-1]
    ARGC -= 4
    fn = "/var/log/cable/event." startdate starthr ".txt"
    print startdate, starthr, findate, finhr
    cstartdate = parsedate( startdate )
    cfindate = parsedate( findate )
    cstartdate = parsedate( startdate )
} {
    namehr = $1
    ch = 0+$2
    db = 0.0+$3
    sub( /[0-9][0-9][0-9]$/, "", namehr )
    hr = namehr
    nameday = namehr
    sub( /^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/, "", hr )
    sub( /[0-9][0-9][0-9]$/, "", nameday )
    hr = converthourtotensmin(0+hr)
    if ( ( ( 0+(nameday "" sprintf("%3.3d",hr))) >= 0+( "" startdate starthr) ) && ( 0+(nameday "" sprintf("%3.3d",hr)) <= 0+("" findate finhr) ) ) {
    asum[nameday,hr,ch] += db
    if ( (nameday,hr,ch) in amax ) {
        if ( db > amax[nameday,hr,ch] ) {
            amax[nameday,hr,ch] = db
        }
    } else {
        amax[nameday,hr,ch] = -1000.0
    }
    if ( (nameday,hr,ch) in amin ) {
        if ( db < amin[nameday,hr,ch] ) {
            amin[nameday,hr,ch] = db
        }
    } else {
        amin[nameday,hr,ch] = 1000.0
    }
    ac[nameday,hr,ch]++
    }
} END {
    sstr = sprintf( "rm %s", fn )
    system( sstr )
    for ( rawdate = startdate; (0+rawdate) <= (0+findate); rawdate = incrawdate(rawdate) ) {
        for ( hr = 0; hr < 24*6; hr++ ) {
            for ( ch = 1; ch <= 32; ch++ ) {
                if ( (rawdate,hr,ch) in ac ) {
                    printf( "%s %3.3d %d %3.2f %3.2f %3.2f\n", rawdate, hr, ch, amin[rawdate,hr,ch], asum[rawdate,hr,ch]/ac[rawdate,hr,ch], amax[rawdate,hr,ch] ) >>fn
                }
            }
        }
    }
}

function converthourtotensmin( h, t, nh ) {
    t = int(h%10)
    nh = int(h/10)
    return(nh*6+t)
}

function parsedate( s, i, cf, yr, m, d, lyr ) {
    yr = int(s/10000)
    m = int((s%10000)/100)
    d = int((s%100)-1)
    lyr=leapyear(yr)
    dpm[2]=27+lyr
    yr *= 365.2422
    yr = int(yr)
    for ( i = 1; i < m; i++ ) {
        yr += dpm[i]+1
    }
    yr += d 
    return( yr )
}

function incrawdate( s, i, cf, yr, m, d, lyr ) {
    yr = int(s/10000)
    m = int((s%10000)/100)
    d = int((s%100)-1)
    lyr=leapyear(yr)
    dpm[2]=27+lyr
    d++
    if ( d > dpm[m] ) {
        m++
        d = 1
        if ( m > 12 ) {
            yr++
            m = 1
        }
    }
    return( "" yr sprintf( "%2.2d", m) sprintf( "%2.2d", d+1 ) )
}

function leapyear(x) {
    return( !(x % 4) + !(x % 100) - !(x % 400) )
}

