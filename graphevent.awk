BEGIN {
    maxday = 0
    minday = 99999999
    gmax = -1000.0
    gmin = +1000.0
    dpm[1]=30
    dpm[2]=27
    dpm[3]=30
    dpm[4]=29
    dpm[5]=30
    dpm[6]=29
    dpm[7]=30
    dpm[8]=30
    dpm[9]=29
    dpm[10]=30
    dpm[11]=29
    dpm[12]=30
    mo["Jan"]=1
    mo["Feb"]=2
    mo["Mar"]=3
    mo["Apr"]=4
    mo["May"]=5
    mo["Jun"]=6
    mo["Jul"]=7
    mo["Aug"]=8
    mo["Sep"]=9
    mo["Oct"]=10
    mo["Nov"]=11
    mo["Dec"]=12
    adb[0]=0.0;acolor[0]="#330"
    adb[1]=-1.0;acolor[1]="#330"
    adb[2]=-3.0;acolor[2]="#ab0"
    adb[3]=-5.0;acolor[3]="#f90"
    adb[4]=-7.0;acolor[4]="#f70"
    adb[5]=-8.0;acolor[5]="#f50"
    adb[6]=-11.0;acolor[6]="#f30"
    adb[7]=-13.0;acolor[7]="#f10"
    adb[8]=2.0;acolor[8]="#330"
    adb[9]=4.0;acolor[9]="#330"
    adb[10]=6.0;acolor[10]="#330"
    adb[11]=8.0;acolor[11]="#330"
    cdb = 12
    datetime = 0 
} {
    if ( datetime == 0 ) {
        datetime = FILENAME
        sub( /^.*event\./, "", datetime )
        datetime = substr(datetime,0,4) "/" substr(datetime,5,2) "/" substr(datetime,7,2) " " sprintf( "%4.4d", 100*int((0+substr(datetime,9,4))/6) )
    }
    svgfn = FILENAME
    sub( /\.txt$/, ".svg", svgfn )
    day=parsedate(0+$1)
    hr=0+$2
    min=0.0+$4
    avg=0.0+$5
    max=0.0+$6
    lastdatetime = substr($1,0,4) "/" substr($1,5,2) "/" substr($1,7,2) " " sprintf( "%4.4d", 100*int((0+hr)/6) )
    if ( !( day in aminhr ) ) {
        aminhr[day] = hr
    }
    amaxhr[day] = hr
    if ( day < minday ) {
        minday = day
    }
    if ( day > maxday ) {
        maxday = day
    }
    if ( max > gmax ) {
        gmax = max
    }
    if ( min < -13.0 ) {
        min = -13.0
    }
    if ( max > 7.0 ) {
        max = 7.0
    }
    if ( min < gmin ) {
        gmin = min
    }
    if ( (day,hr) in amax ) {
        if ( max > amax[day,hr] ) {
            amax[day,hr] = max
        }
    } else {
        amax[day,hr] = max
    }
    if ( (day,hr) in amin ) {
        if ( min < amin[day,hr] ) {
            amin[day,hr] = min
        }
    } else {
        amin[day,hr] = min
    }
    if ( (day,hr) in aavg ) {
        aavg[day,hr] += avg
        acount[day,hr]++
    } else {
        acount[day,hr] = 1
        aavg[day,hr] = avg
    }
} END {
    print "<?xml version=\"1.0\" standalone=\"yes\"?>" >svgfn
    close( svgfn )
    print "<svg version=\"1.1\" viewBox=\"0 0 1920 1080\" width=\"100%\" xmlns=\"http://www.w3.org/2000/svg\" style=\"background-color:#fff\">" >>svgfn
    close( svgfn )
#    printf( "<text font-weight=\"bold\" font-size=\"60\" font-style=\"italic\" text-anchor=\"center\" x=\"320\" y=\"150\" fill=\"#000\">%s</text>", "Thanks Wave Broadband! Algorithm change? made crappy levels not as big an issue" ) >>svgfn
#    close( svgfn )
    printf( "<text font-weight=\"bold\" font-size=\"80\" font-style=\"italic\" text-anchor=\"center\" x=\"720\" y=\"300\" fill=\"#f77\">%s</text>", datetime ) >>svgfn
    close( svgfn )
    printf( "<text font-weight=\"bold\" font-size=\"80\" font-style=\"italic\" text-anchor=\"center\" x=\"720\" y=\"780\" fill=\"#f77\">%s</text>", lastdatetime ) >>svgfn
    close( svgfn )
    for ( i = minday; i <= maxday; i++ ) {
        for ( j = 0; j < 24; j++ ) {
            gridlinetime( i, j*6, " " j, "#f80", "#444" )
        }
        gridlinetime( i, 0, "" (i-maxday) "day", "#320", "#aaa" )
        gridlinetime( i, 12*6, "noon", "#740", "#777" )
        gridlinetime( i, 18*6, "6pm", "#f80", "#444" )
        gridlinetime( i, 6*6, "6am", "#f80", "#444" )
    }
    for ( i = 0; i < cdb; i++ ) {
        gridlinedb( adb[i], acolor[i] )
    }
    drawaverage()
    drawmaximum()
    drawminimum()
    print "</svg>" >>svgfn
    close( svgfn )
    sstr = sprintf( "mv %s /var/www/html/", svgfn )
    system( sstr )
}

function drawaverage( day, hr ) {
    print "<path stroke-width=\"2\" stroke=\"#0a0\" fill=\"none\" " >>svgfn
    close( svgfn )
    print "d=\"" >>svgfn
    close( svgfn )
    printf( "M %5.4f %5.4f\n", scaledayhour( minday, aminhr[minday] ), scaledb( aavg[minday,aminhr[minday]]/acount[minday,aminhr[minday]]) ) >>svgfn
    close( svgfn )
    for ( day = minday; day <= maxday; day++ ) {
        if ( day in aminhr ) {
        for ( hr = aminhr[day]; hr <= amaxhr[day]; hr++ ) {
            if ( (day,hr) in aavg ) {
    printf( "L %5.4f %5.4f\n", scaledayhour( day, hr ), scaledb( aavg[day,hr]/acount[day,hr] ) ) >>svgfn
    close( svgfn )
            }
        }
        }
    }
    printf( "\"/>\n" ) >>svgfn
    close( svgfn )
}

function drawminimum( day, hr ) {
    print "<path stroke-width=\"2\" stroke=\"#f00\" fill=\"none\" " >>svgfn
    close( svgfn )
    print "d=\"" >>svgfn
    close( svgfn )
    printf( "M %5.4f %5.4f\n", scaledayhour( minday, aminhr[minday] ), scaledb( amin[minday,aminhr[minday]] ) ) >>svgfn
    close( svgfn )
    for ( day = minday; day <= maxday; day++ ) {
        if ( day in aminhr ) {
        for ( hr = aminhr[day]; hr <= amaxhr[day]; hr++ ) {
            if ( (day,hr) in aavg ) {
    printf( "L %5.4f %5.4f\n", scaledayhour( day, hr ), scaledb( amin[day,hr] ) ) >>svgfn
    close( svgfn )
            }
        }
        }
    }
    printf( "\"/>\n" ) >>svgfn
    close( svgfn )
}

function drawmaximum( day, hr ) {
    print "<path stroke-width=\"2\" stroke=\"#a3a\" fill=\"none\" " >>svgfn
    close( svgfn )
    print "d=\"" >>svgfn
    close( svgfn )
    printf( "M %5.4f %5.4f\n", scaledayhour( minday, aminhr[minday] ), scaledb( amax[minday,aminhr[minday]] ) ) >>svgfn
    close( svgfn )
    for ( day = minday; day <= maxday; day++ ) {
        if ( day in aminhr ) {
        for ( hr = aminhr[day]; hr <= amaxhr[day]; hr++ ) {
            if ( (day,hr) in aavg ) {
    printf( "L %5.4f %5.4f\n", scaledayhour( day, hr ), scaledb( amax[day,hr] ) ) >>svgfn
    close( svgfn )
            }
        }
        }
    }
    printf( "\"/>\n" ) >>svgfn
    close( svgfn )
}

function scaledayhour( day, hr ) {
    return( 1920.0 * ( day + (hr/144.0) - (minday + aminhr[minday]/144.0) ) / ( ( maxday + amaxhr[maxday]/144.0) - ( minday + aminhr[minday]/144.0) ) )
}

function scaledb( db ) {
    return( (1080.0 - ( 1080.0 * ( db - gmin ) / ( gmax - gmin ) ) ) )
}

function parsedate( s, i, cf, yr, m, d, lyr ) {
    yr = int(s/10000)
    m = (s%10000)/100
    d = (s%100)-1
    lyr=leapyear(yr)
    dpm[2]=27+lyr
    yr *= 365.2422
    yr = int(yr)
    for ( i = 1; i < m; i++ ) {
        yr += dpm[i]+1
    }
    yr += d 
    return( yr )
}

function leapyear(x) {
    return( !(x % 4) + !(x % 100) - !(x % 400) )
}

function gridlinedb( db, color ) {
    printf( "<text font-weight=\"bold\" font-size=\"20\" font-style=\"italic\" text-anchor=\"right\" x=\"%5.4f\" y=\"%5.4f\" fill=\"%s\">%3.1f dBmV</text>", scaledayhour( minday, aminhr[minday] ), scaledb( db ), color, db ) >>svgfn
    close( svgfn )
    printf( "<path stroke-width=\"2\" stroke=\"%s\" ", color ) >>svgfn
    close( svgfn )
    print "d=\"" >>svgfn
    close( svgfn )
    printf( "M %5.4f %5.4f ", scaledayhour( minday, aminhr[minday] ), scaledb( db ) ) >>svgfn
    close( svgfn )
    printf( "L %5.4f %5.4f ", scaledayhour( maxday, amaxhr[maxday] ), scaledb( db ) ) >>svgfn
    close( svgfn )
    printf( "\"/>\n" ) >>svgfn
    close( svgfn )
}

function gridlinetime( day, hr, tag, tagcolor, color ) {
    printf( "<text font-weight=\"bold\" font-size=\"20\" font-style=\"italic\" text-anchor=\"right\" x=\"%5.4f\" y=\"%5.4f\" fill=\"%s\">%s</text>", scaledayhour( day, hr ), scaledb( gmin ), tagcolor, tag ) >>svgfn
    printf( "<text font-weight=\"bold\" font-size=\"20\" font-style=\"italic\" text-anchor=\"right\" x=\"%5.4f\" y=\"%5.4f\" fill=\"%s\">%s</text>", scaledayhour( day, hr ), scaledb( gmax )+20, tagcolor, tag ) >>svgfn
    close( svgfn )
    printf( "<path stroke-width=\"2\" stroke=\"%s\" ", color ) >>svgfn
    close( svgfn )
    print "d=\"" >>svgfn
    close( svgfn )
    printf( "M %5.4f %5.4f ", scaledayhour( day, hr ), scaledb( gmin ) ) >>svgfn
    close( svgfn )
    printf( "L %5.4f %5.4f ", scaledayhour( day, hr ), scaledb( gmax ) ) >>svgfn
    close( svgfn )
    printf( "\"/>\n" ) >>svgfn
    close( svgfn )
}

